var botaoAdicionar = document.querySelector("#buscar-pacientes");

botaoAdicionar.addEventListener("click", function(){
	console.log("Buscando pacientes");
	var xhr = new XMLHttpRequest();
	xhr.open("GET", "https://api-pacientes.herokuapp.com/pacientes1111");
	xhr.addEventListener("load", function() {
		var erroAjax = document.querySelector("#erro-ajax");
		if (xhr.status == 200) {
			console.log(xhr.responseText);
			var resposta = xhr.responseText;
			var pacientes = JSON.parse(resposta);
			pacientes.forEach(function(paciente) {
				adicionaPacienteNaTabela(paciente);
			});
		} else {
			erroAjax.classList.remove("invisivel");
		}
	});
	xhr.send();
});